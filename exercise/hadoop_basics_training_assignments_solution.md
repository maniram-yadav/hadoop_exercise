## HDFS Assignments

**a.**	Create a text file userdata.txt in your home directory having following lines using a text editor:

I am working on a Linux Server. 
I am learning to work on Hadoop ecosystem.



**b.**	Create a folder called training under your home directory in HDFS

Write the command used here:      **hadoop fs -mkdir /training**



**c.**	Create a folder called SampleData under training

Write the command used here:      **hadoop fs -mkdir /training/Sampledata**



**d.**	Copy file userdata.txt to SampleData folder in HDFS

Write the command used here:      **hadoop fs -copyFromLocal userdata.txt /training/Sampledata/userdata.txt**



**e.**	Display the content of userdata.txt in hdfs using cat command

Write the command used here:        **hadoop fs -cat /training/Sampledata/userdata.txt**



**f.**	Create another directory called SampleDataBak under training directory in HDFS

Write the command used here:        **hadoop fs -mkdir /training/SampleDataBak**



**g.**	Copy the file userdata.txt from SampleData folder to SampleDataBak:

Write the command used here:       **hadoop fs -cp  /training/Sampledata/userdata.txt  /training/SampleDataBak/userdata.txt**



**h.**	List all sub-folders and files under training folder in HDFS:

Write the command used here:       **hadoop fs -ls /training**



**i.**	Display the total disk space used by training directory

Write the command used here:        **hadoop fs -du -s -h /training**



**j.**	Display the disk space used by each folder under training directory

Write the command used here:         **hadoop fs -du -h /training**



**k.**	Change the Replication factor of training/SampleData/userdata.txt to 2

Write the command used here:        **hadoop fs -setrep -w 2 /training/SampleData/userdata.txt**



l.	Browse the training folder in HDFS using Web UI (localhost:50070) and note down the following:
File / Folder Name			Size		Replication		Block Size	
training
userdata.txt (under SampleData)
userdata.txt (under SampleDataBak)



**m.**	Delete training folder along with all sub folders and files:

Write the command used here:        **hadoop fs -rm -R /training**



![HDFS assignment screenshot](images/hadoop1.png)
![HDFS assignment screenshot](images/hadoop2.png)




## Hive Assignments

Input Data

Employee.txt - has the following columns - `EmpID, Name, Band, DepartmentID, Salary`

**Employee.txt**


`A1001,Ramesh,B1,IT,40000`

`A1002,Ganesh,B2,HR,35000`

`A1003,Latha,B1,HR,30000`

`A1008,Shirish,B2, IT,55000`

`A1009,Shibu,B2, MKTG,48000`


EmpProj.txt - has following columns – `EmpID, projectID, year-week, EffortHrs`

**EmpProj.txt**

`A1001,IDW,201601,40`

`A1002,IDW,201601,45`

`A1003,GDW,201601,25`

`A1008,IDW,201601,35`

`A1009,GDW,201601,50`

`A1001,IDW,201602,45`

`A1002,IDW,201602,48`

`A1003,GDW,201602,45`

`A1008,IDW,201603,50`

`A1009,GDW,201602,40`

`A1001,IDW,201602,45`

`A1002,IDW,201602,49`

`A1003,GDW,201602,46`

`A1009,GDW,201602,45`

`A1002,CDW,201603,50`

`A1002,CDW,201604,50`



Department.txt – has following columns – `DepartmentID, Department Name`

**Department.txt**


`IT,Information Technology`

`HR,Human Resources`

`MKTG, Marketing`


### Assignment 1: Create a Managed internal table

**a.**	Connect to Hive CLI

**b.**	Create a database called projectdb  :  **create database projectdb;**

**c.**	Create a table employee to store data in employee.txt under projectdb

**create table employee(EmpID string,Name string, Band string, DepartmentID string, Salary int)
COMMENT 'Employee details'
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
STORED AS TEXTFILE;**

**d.**	List the structure of employee table

Write the command used here: **describe employee;**

**e.**	List all storage parameters of employee table using describe command.

Write the command used here: 

**describe extended employee;**,  


**f.**	List data in employee table using select statement (make sure that the output has column header s)

Write the commands used here:  
**set hive.cli.print.header=true;**
**select * from employee;**

![Hive assignment screenshot](images/hive11.png)
![Hive assignment screenshot](images/hive12.png)

>
>

###  Assignment 2: Create External Table and execute Join query

**a.**	Copy file Department.txt to folder dept under your home directory in HDFS
**hadoop fs -copyFromLocal /home.maniram/Department.txt /dept/department.txt**

**b.**	Create an external Hive table department to read data from dept folder in hdfs

**create external table department(DepartmentID string, Department_Name string)
COMMENT 'Department details'
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
LOCATION '/dept;**

**c.**	Write a join query to join department and employee tables and get the following output:

**select department_name,salary from employee e join department d on e.departmentid=d.departmentid;**

Department Name, Total Salary

**d.**	How many mappers and reducers are executed in the map reduce job executed by Hive?

Number of Mappers: 1

Number of reducers: 1

**e.**	Display the explain plan for the join query using explain statement

**explain select department_name,salary from employee e join department d on e.departmentid=d.departmentid;**


![Hive assignment screenshot](images/hive21.png)
![Hive assignment screenshot](images/hive22.png)
![Hive assignment screenshot](images/hive23.png)

>
>

###  Assignment 3: Create a Partitioned Table and load data

**a.**	Create a table project_details partitioned by project ID and having following columns:
`EmpID, year-week, EffortHrs`



**b.**	Load data from file empProj.txt into table project_details


**c.**	Write a select query to get total effort spent by empID = A1002 by project name.

Required Output columns: projectID, empID, empName, totalEffort



**create table empprojects(EmpID string, projectID string,year_week int, EffortHrs int)  COMMENT 'Employee projects' ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' STORED AS TEXTFILE;**


**create table project_details(EmpID string,year_week int, EffortHrs int) PARTITIONED BY (projectID string) ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' STORED AS TEXTFILE;**

**set hive.exec.dynamic.partition.mode=nonstrict;**

**load data local inpath '/home/maniram/EmpProj.txt' overwrite into table empprojects;**

**insert overwrite table project_details PARTITION(projectid) select empid,year_week,efforthrs,projectid from empprojects;**

**select p.projectid, e.empid, e.name,sum(p.efforthrs)  from project_details p join employee e on p.empid=e.empid where e.empid='A1002' group by p.projectid,e.empid, e.name;**




![Hive assignment screenshot](images/hive31.png)
![Hive assignment screenshot](images/hive32.png)
![Hive assignment screenshot](images/hive34.png)

>
>